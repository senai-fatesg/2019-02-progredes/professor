import json

data= {}
opcao = 's'
while opcao == 's':
    chave = input("Digite a chave: ")
    valor = input("Digite o valor: ")
    data[chave] = valor
    opcao = input("Continua ? (s/n) ")

with open('data.json', 'w') as outfile:
    json.dump(data, outfile)