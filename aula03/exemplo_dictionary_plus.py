p = {
	'nome': 'Maria',
	'endereco': {
		'logradouro': 'Rua 1',
		'numero': '12',
		'bairro': 'centro',
		'municipio': 'Goiania',
		'uf': 'GO',
		'cep': '74345-12'
	}
}

print(p)
print('\n---\n')
end = p['endereco']
print(end)
print('\n---\n')
print(end['bairro'])

