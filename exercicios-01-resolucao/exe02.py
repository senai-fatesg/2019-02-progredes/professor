'''
02. Faça um programa, com uma função que necessite de um argumento. A função retorna o valor de caractere ‘P’, se seu argumento for positivo, e ‘N’, se seu argumento for zero ou negativo.
'''

def verifica(n):
    if n > 0:
        return 'P'
    else:
        return 'N'

print(verifica(10))
print(verifica(0))
print(verifica(-12))

