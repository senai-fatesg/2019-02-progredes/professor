dict = {
    'nome': 'Francisco Calaça', 
    'idade': 38, 
    'telefone': '3232-3232', 
    'endereco': 'Rua 2 Qd 01 Lt 03'}
# ------- IMPRIMINDO UM A UM ------------
print('Nome:', dict['nome'])
print('Idade:', dict['idade'])
print('Telefone:', dict['telefone'])
print('Endereço:', dict['endereco'])

# ------- IMPRIMINDO TODOS --------------
print(dict)

# ------- PERGUNTANDO      --------------
chave = input('qual informação deseja reimprimir: ')
print(dict[chave])
