from flask import Flask, render_template, request
import database as db


app = Flask(__name__)
db.criarTabela()

@app.route("/", methods=['GET'])
def root():
    clientes = db.listar()
    return render_template('formulario.html', clientes=clientes, cliente={})

@app.route("/excluir", methods=['GET'])
def excluir():
    id = request.args.get('id')
    db.excluir(id)
    clientes = db.listar()
    return render_template('formulario.html', clientes=clientes, cliente={})

@app.route("/editar", methods=['GET'])
def editar():
    id = request.args.get('id')
    cliente = db.consultar(id) 
    clientes = db.listar()
    return render_template('formulario.html', clientes=clientes, cliente=cliente)

@app.route("/", methods=['POST'])
def salvar():
    nome = request.form.get('nome')
    telefone = request.form.get('telefone')
    print(nome, telefone)
    mensagem = ''
    if not nome:
        mensagem = 'O nome é obrigatório'
    elif not telefone:
        mensagem = 'O telefone é obrigatório'
    else:
        id = request.form.get('id')
        if id:
            db.alterar(id, nome, telefone)
        else:
            db.inserir(nome, telefone)

    clientes = db.listar()

    return render_template('formulario.html', clientes=clientes, mensagem=mensagem, cliente={})


app.run(host='0.0.0.0', debug=True,port=8080)
