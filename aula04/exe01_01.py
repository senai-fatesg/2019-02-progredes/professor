dict = {}
dict['nome'] = input("Digite o nome: ")
dict['idade'] = input("Digite o idade: ")
dict['endereco'] = input("Digite o endereço: ")
dict['telefone'] = input("Digite o telefone: ")

# ------- IMPRIMINDO UM A UM ------------
print('Nome:', dict['nome'])
print('Idade:', dict['idade'])
print('Telefone:', dict['telefone'])
print('Endereço:', dict['endereco'])

# ------- IMPRIMINDO TODOS --------------
print(dict)

# ------- PERGUNTANDO      --------------
chave = input('qual informação deseja reimprimir: ')
print(dict[chave])

dict['Sexo'] = input('Digite o sexo')
print(dict)
