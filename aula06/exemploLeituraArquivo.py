#Forma 1. Menos aconselhavel.
f = open("arquivo2.txt")
lines = f.read().splitlines()
for line in lines:
    print(line)

print("-------------------------")

#Forma 2. Recomendada!
with open("arquivo2.txt") as f:
    lines = f.read().splitlines()
    for line in lines:
        print(line)