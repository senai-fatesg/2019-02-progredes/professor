n1str = input("Digite n1")
n2str = input("Digite n2")

n1 = float(n1str)
n2 = float(n2str)

media = (n1 + n2) / 2

print("A média entre", n1str, "e", n2str, "é:", media)
print("A média entre " + n1str + " e " + n2str + " é: " + media)
print("A média entre {} e {} é: {}".format(n1str, n2str, media))