# Configuração do Python com Telegram

## Baixar ambiente

Você tem duas opções para executar este exemplo.

1. Instalar a biblioteca e executar o arquivo echobot.py contido na raiz do repo

1. Baixar todo o ambiente deste repositório e instalar localmente.

## Opção 1: Instalar a biblioteca

```bash
pip install python-telegram-bot --upgrade
```

Modifique o token dentro de echobot.py

Execute 

```bash
python echobot.py
```


## Baixar ambiente com download.

Baixe este repositório, entre no diretório **python-telegram-bot** e execute:

```bash
python setup.py install
```

Abra o arquivo **examples/echo.py** e modifique o tokey

Execute o arquivo **examples/echo.py**

## Obtenção do token

Acesso o botfather pelo telegram *@botfather*

Execute **/newbot**

Coloque o nome do bot

Coloque o usuário do bot finalizando com **_bot**