p = {
	'nome': 'Francisco',
	'endereco': {
		'logradouro': 'Rua 3',
		'numero': '31',
		'bairro': 'Centro',
		'municipio': 'Goiania',
		'uf': 'GO',
		'cep': '74123-123'
	},
	'filhos': [{
			'nome': 'Pedro',
			'endereco': {
				'logradouro': 'Rua 3',
				'numero': '31',
				'bairro': 'Centro',
				'municipio': 'Goiania',
				'uf': 'GO',
				'cep': '74123-123'
			}
		}, {
			'nome': 'Joao',
			'endereco': {
				'logradouro': 'Rua 3',
				'numero': '31',
				'bairro': 'Centro',
				'municipio': 'Goiania',
				'uf': 'GO',
				'cep': '74123-123'
			}
		}
	]
}

print(p['nome'], p['filhos'][0]['nome'], p['filhos'][1]['nome'])

print('\n--\n')
print(p['nome'])
print(p['filhos'][0]['nome'])
print(p['filhos'][1]['nome'])

print('\n--\n')
print('\nFilhos')
for filho in p['filhos']:
    print(filho['nome'])