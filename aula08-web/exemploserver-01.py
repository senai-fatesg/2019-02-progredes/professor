from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/oi')
def oi():
    return "Oi"    


app.run(host='0.0.0.0', debug=True,port=8080)
