from flask import Flask, render_template, request
import ceplib

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template('consultaCep.html')

@app.route("/cep")
def getCep():
    cepcons = request.args.get('cep')
    cep = ceplib.consultarCep(cepcons)
    print(cep)
    return render_template('cep.html', cep=cep)


app.run(host='0.0.0.0', debug=True,port=8080)
