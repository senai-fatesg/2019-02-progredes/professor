# pip install pysqlite3 

import sqlite3

def criarTabela(db_con):
    cursor = db_con.cursor()

    # criando a tabela (schema)
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS cliente (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            nome TEXT NOT NULL,
            telefone TEXT
    );
    """)

    print('Tabela criada com sucesso.')

def inserir(db_con, nome, telefone):
    cursor = db_con.cursor()

    # inserindo dados na tabela
    cursor.execute(f"""
    INSERT INTO cliente (nome, telefone) VALUES ('{nome}', '{telefone}')
    """)
    db_con.commit()

def listar(db_con):
    result = []
    cursor = db_con.cursor()
    cursor.execute("SELECT id, nome, telefone FROM cliente;")
    for linha in cursor.fetchall():
        cli = {}
        cli['nome'] = linha[1]
        cli['telefone'] = linha[2]
        result.append(cli)
    return result


conn = sqlite3.connect('clientes.db')
criarTabela(conn)

nome = input('Digite o nome:')
telefone = input('Digite o telefone:')

inserir(conn, nome, telefone)

clientes = listar(conn)

for c in clientes:
    print(c)

conn.close()