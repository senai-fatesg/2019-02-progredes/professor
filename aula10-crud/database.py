# pip install pysqlite3 

import sqlite3

def criarTabela():
    db_con = conectar()
    cursor = db_con.cursor()

    # criando a tabela (schema)
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS cliente (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            nome TEXT NOT NULL,
            telefone TEXT
    );
    """)

    print('Tabela criada com sucesso.')
    db_con.close()

def inserir(nome, telefone):
    db_con = conectar()
    cursor = db_con.cursor()

    # inserindo dados na tabela
    cursor.execute(f"""
    INSERT INTO cliente (nome, telefone) VALUES ('{nome}', '{telefone}')
    """)
    db_con.commit()
    db_con.close()

def alterar(id, nome, telefone):
    db_con = conectar()
    cursor = db_con.cursor()

    # inserindo dados na tabela
    cursor.execute(f"""
    UPDATE cliente set nome = '{nome}', telefone = '{telefone}' WHERE id = {id} 
    """)
    db_con.commit()
    db_con.close()

def excluir(id):
    db_con = conectar()
    cursor = db_con.cursor()

    # inserindo dados na tabela
    cursor.execute(f"""
    DELETE FROM cliente WHERE id = {id}
    """)
    db_con.commit()
    db_con.close()

def listar():
    db_con = conectar()
    result = []
    cursor = db_con.cursor()
    cursor.execute("SELECT id, nome, telefone FROM cliente;")
    for linha in cursor.fetchall():
        cli = {}
        cli['id'] = linha[0]
        cli['nome'] = linha[1]
        cli['telefone'] = linha[2]
        result.append(cli)
    db_con.close()
    return result

def consultar(id):
    db_con = conectar()
    cursor = db_con.cursor()
    cursor.execute(f"SELECT id, nome, telefone FROM cliente WHERE id = {id};")
    linha = cursor.fetchone()
    cli = {}
    cli['id'] = linha[0]
    cli['nome'] = linha[1]
    cli['telefone'] = linha[2]
    db_con.close()
    return cli

def conectar():
    return sqlite3.connect('clientes.db')


