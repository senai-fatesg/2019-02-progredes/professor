from urllib.parse import urlencode
from urllib.request import Request, urlopen

def unescapeXml(s):
    s = s.replace("&lt;", "<")
    s = s.replace("&gt;", ">")
    s = s.replace("&amp;", "&")
    s = s.replace("&nbsp;", " ")
    return s

def unescapeString(s):
    s = s.replace('\\r', '')
    s = s.replace('\\t', '')
    s = s.replace('\\n', '')
    return s

def getposicao(texto, inicio, fim):
    pi = texto.index(inicio) + len(inicio)
    pf = texto.index(fim)
    res = texto[pi : pf]
    return res

def consultarCep(cep):
    parametros = {"relaxation": cep, "tipoCEP": "ALL", "semelhante": "N"}

    url = "http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm"

    req =  Request(url, urlencode(parametros).encode())
    result = urlopen(req).read()
    result = str(result)

    result = unescapeString(result)
    result = bytes(result, "iso-8859-1").decode("unicode_escape")
    result = unescapeXml(result)

    # print(result)
    importa = getposicao(result, "CEP:", "</table>")
    rua = getposicao(importa, 'width="150">', '</td>')
    importa = getposicao(importa, '</td>', '</td></tr>')
    bairro = getposicao(importa, '<td>', '</td>')
    cidade = getposicao(importa, '</td><td>', '</td><td width="55"')

    cep_consultado = {'rua': rua, 'bairro': bairro, 'cidade': cidade}
    return cep_consultado