valor1 = input("Digite um valor: ")
print("O valor digitado foi", valor1)
valor1_int = int(valor1)
print("O tipo da variavel valor1 é", type(valor1))
print("O tipo da variavel valor1_int é", type(valor1_int))

valor2 = input("Digite outro valor: ")
print("O valor digitado foi", valor2)
valor2_int = int(valor2)
print("O tipo da variavel valor2 é", type(valor2))
print("O tipo da variavel valor2_int é", type(valor2_int))

soma = valor1 + valor2
print("A soma dos valores é", soma)

soma = valor1_int + valor2_int
print("A soma dos valores (convertidos) é", soma)
