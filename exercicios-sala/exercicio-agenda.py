import json
from os import path

contatos = {}

if path.exists('dados.json'):
    with open('dados.json') as json_file:
        data = json.load(json_file)
        contatos.update(data)
else:
    contatos['dados'] = []

cont = {}
cont['nome'] = input('Digite o nome:')    
cont['telefone'] = input('Digite o telefone:')

contatos['dados'].append(cont)

with open('dados.json', 'w') as outfile:
    json.dump(contatos, outfile)