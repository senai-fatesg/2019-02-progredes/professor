import string
from random import choice

def GenPasswd1(len):
   passwd = ''
   chars = string.ascii_letters + string.digits
   for i in range(len):
      passwd = passwd + choice(chars)
   return passwd

def GenPasswd2(length, chars):
   return ''.join([choice(chars) for i in range(length)])

pass1 = GenPasswd1(20)
print(f'Senha gerada: {pass1}')

print(string.ascii_letters)

pass2 = GenPasswd2(20, string.ascii_letters + '!@#$%^&*()-_+=')
print(f'Senha gerada: {pass2}')