# criacao do array com 3 elementos
pessoas = ['Maria', 'Antonio', 'Jose']

print(pessoas)

novo_nome = input('Digite um nome: ')

# inclusao de um novo elemento no array
pessoas.append(novo_nome)

print(pessoas)

for n in pessoas:
    print(n, ' Cadastro com sucesso...', end='')
